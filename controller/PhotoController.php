<?php

require_once ROOT . '/model/Photo.php';
require_once ROOT . '/model/User.php';

class PhotoController extends Controller
{
	private $_photos_per_page = 12;

	public function actionIndex()
	{
		if (isset($_POST['delete']) && isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
		{
			$photo = Photo::getPhotoById($_POST['del_id']);
			$path = $photo['photo_src'];
			$path = str_replace('/camagru/', '', $path);
			if (file_exists($path))
				unlink($path);
			Photo::deletePhotoFromDb($_POST['del_id'], $_SESSION['logged_on_user']);
		}
		else if (isset($_POST['like']) && !empty($_SESSION['logged_on_user']))
		{
			$photoItem = Photo::getPhotoById($_POST['like_id']);
			$photoAuthor = User::getUserByLogin($photoItem['photo_user']);
			if ($photoAuthor['user_login'] != $_SESSION['logged_on_user'])
			{
				$msg = "someone has just liked your photo: https://" . SITE_HOST . SITE_BASE_URI . "photos/"
					. $_POST['like_id'];
				mail($photoAuthor['user_email'], 'camagru', $msg, 'sotyaniku@gmail.com');
			}
			Photo::addLikeToPhoto($_POST['like_id']);
			echo "A like to photo has been added";
			return true;
		}
		$photos_num = Photo::getPhotoNum();
		$_SESSION['pages_num'] = ceil($photos_num / $this->_photos_per_page);
		$start_num = $this->_getStartNum();
		$photoList = array();
		$photoList = Photo::getPhotoList($start_num, $this->_photos_per_page);
		$this->view->generate('PhotoView.php', 'TemplateView.php', $photoList);
		return true;
	}

	private function _getStartNum()
	{
		$page_num = 1;
		if (isset($_GET['page']) && $_GET['page'] > 1)
			$page_num = $_GET['page'];
		$_SESSION['cur_page'] = $page_num;
		$start_num = ($page_num - 1) * $this->_photos_per_page;
		return $start_num;
	}

	public function actionView($id)
	{
		if (isset($_POST['delete']) && isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
		{
			$photo = Photo::getPhotoById($_POST['del_id']);
			$path = $photo['photo_src'];
			$path = str_replace('/camagru/', '', $path);
			if (file_exists($path))
				unlink($path);
			Photo::deletePhotoFromDb($_POST['del_id'], $_SESSION['logged_on_user']);
			header('Location: /camagru/photos');
		}
		else if (isset($_POST['like']) && isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
		{
			$photoItem = Photo::getPhotoById($_POST['like_id']);
			$photoAuthor = User::getUserByLogin($photoItem['photo_user']);
			if ($photoAuthor['user_login'] != $_SESSION['logged_on_user'])
			{
				$msg = "someone has just liked your photo: https://" . SITE_HOST . SITE_BASE_URI . "photos/"
					. $_POST['like_id'];
				mail($photoAuthor['user_email'], 'camagru', $msg, 'sotyaniku@gmail.com');
			}
			Photo::addLikeToPhoto($_POST['like_id']);
		}
		else if (isset($_POST['comment']) && isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
		{
			date_default_timezone_set('Europe/Kiev');
			$time = date("Y-m-d H:i:s");
			Photo::addCommentToPhoto($id, nl2br($_POST['comment']),
				$_SESSION['logged_on_user'], $time);
			$photoItem = Photo::getPhotoById($id);
			$photoAuthor = User::getUserByLogin($photoItem['photo_user']);
			if ($photoAuthor['user_login'] != $_SESSION['logged_on_user'])
			{
				$msg = "someone has just commented your photo: https://" . SITE_HOST . SITE_BASE_URI . "photos/" . $id;
				mail($photoAuthor['user_email'], 'camagru', $msg, 'sotyaniku@gmail.com');
			}
		}
		if ($id)
		{
			$photoItem = Photo::getPhotoItemById($id);
			$this->view->generate('CommentsView.php', 'TemplateView.php', $photoItem);
		}
		return true;
	}
}
