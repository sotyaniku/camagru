<?php

require_once ROOT . '/model/User.php';
require_once ROOT . '/model/Photo.php';

class UserController extends Controller
{
	private $login;
	private $password;
	private $email;
	private $subject = "Camagru verification";
	private $message = "Hi! To activate your account please visit https://" . SITE_HOST . SITE_BASE_URI . "register/";
	private $from = 'sotyaniku@gmail.com';

	public function actionIndex()
	{
		if (array_key_exists('img', $_POST))
		{
			$this->_saveImage();
			echo "Image has been saved";
		}
		else if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
			$this->view->generate('MainView.php', 'TemplateView.php', null);
		else
		{
			$error_msg = "You shell not pass! Access restricted to authorized users only";
			$this->view->generate('ErrorView.php', 'TemplateView.php', $error_msg);
		}
	}

	private function _saveImage()
	{
		date_default_timezone_set('Europe/Kiev');
		$time = date("Y-m-d H:i:s");
		$img = $this->_convertImage($_POST['img']);
		$img_url = "webroot/images/" . uniqid() . ".jpeg";
		$filter_url = $_POST['filter'];
		preg_match('@https:\/\/' . SITE_HOST . SITE_BASE_URI . '(.+)@', $filter_url, $matches);
		$filter_url = $matches[1];
		$im = imagecreatefromstring($img);
		imagesavealpha($im, 1);
		$stamp = imagecreatefrompng($filter_url);
		$stamp = imagescale($stamp, 640);
		imagecopy($im, $stamp, 0, 0, 0, 0, 640, 480);
		imagepng($im, $img_url);
		Photo::addPhoto($_SESSION['logged_on_user'], SITE_BASE_URI . $img_url, $time);
		imagedestroy($im);
		imagedestroy($stamp);
	}

	private function _convertImage($src)
	{
		preg_match('@url\(\"data:image\/\w+;base64,(.+)\"\)@', $src, $matches);
		$src = chunk_split(str_replace(' ', '+', $matches[1]));
		$img = base64_decode($src);
		return ($img);
	}

	public function actionRegister()
	{
		$error_msg = null;
		if (array_key_exists('register', $_POST))
		{
			if ($this->_findUserInDb($_POST['login']))
				$error_msg = "A user with such login already exists";
			else if ($this->_checkEmailInDb($_POST['email']))
				$error_msg = "A user with such email already exists";
			else
			{
				$this->login = $_POST['login'];
				$this->password = hash('whirlpool', $_POST['passwd']);
				$this->email = $_POST['email'];
				User::addUserToDb($this->login, $this->password, $this->email, 0);
				mail($this->email, $this->subject, $this->message . hash('whirlpool', $this->email),
					$this->from);
			}
		}
		$this->view->generate('RegisterView.php', 'TemplateView.php', $error_msg);
	}

	public function actionConfirm($mail)
	{
		$userList = User::getUserList();
		foreach ($userList as $person)
		{
			if (hash('whirlpool', $person['email']) === $mail)
			{
				User::confirmUserRegistration($person['login']);
				header('Location: /camagru/');
			}
		}
	}

	public function actionLogin()
	{
		$_SESSION['forgot_password'] = 0;
		if (isset($_POST['login']) && isset($_POST['passwd']) && isset($_POST['submit']))
		{
			if ($this->_checkUserAuth($_POST['login'], hash('whirlpool', $_POST['passwd'])) && $_POST['submit'] == 'OK')
				$_SESSION['logged_on_user'] = $_POST['login'];
			else
				$_SESSION['forgot_password'] = 1;
		}
		else if (isset($_POST['submit']) && $_POST['submit'] === 'Logout')
			$_SESSION['logged_on_user'] = '';
		header('Location: ' . $_SESSION['uri']);
	}

	public function actionSendpass()
	{
		$error_msg = null;
		if (array_key_exists('sendmail', $_POST))
		{
			$error_msg = "It seems you are not registered yet";
			$userList = User::getUserList();
			foreach ($userList as $person)
			{
				if ($person['email'] === $_POST['email'])
				{
					$this->password = $this->_createRandomPassword();
					$this->email = $_POST['email'];
					$this->subject = "Camagru password update";
					$this->message = "Your password has been updated to '$this->password'";
					User::updateUserPassword($person['login'], $this->password);
					mail($this->email, $this->subject, $this->message, $this->from);
					$error_msg = null;
					break;
				}
			}
		}
		$this->view->generate('EmailView.php', 'TemplateView.php', $error_msg);
	}

	private function _createRandomPassword()
	{
		$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$charLength = strlen($chars) - 1;
		$randomPass = '';
		for ($i = 0; $i < 8; $i++)
			$randomPass .= $chars[rand(0, $charLength)];
		if (ctype_digit($randomPass))
			$randomPass .= $chars[rand(10, $charLength)];
		else if (ctype_alpha($randomPass))
			$randomPass .= $chars[rand(0, 9)];
		return $randomPass;
	}

	private function _findUserInDb($login)
	{
		$user = User::getUserByLogin($login);
		return $user;
	}

	private function _checkEmailInDb($email)
	{
		$user = User::getUserByEmail($email);
		return $user;
	}

	private function _checkUserAuth($login, $password)
	{
		if (!($user = User::getUserByLogin($login)))
			return 0;
		return ($user['user_login'] == $login && $user['user_password'] == $password && $user['registered']);
	}
}
