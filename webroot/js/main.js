var video = document.querySelector('video');
var canvas = document.getElementById('hidden');
var ctx = canvas.getContext('2d');
var img_src;
var filter_src;

function hasGetUserMedia() {
    return (navigator.getUserMedia);
}

if (hasGetUserMedia()) {
    console.log('everything is ok');
    var constraints = {video: {facingMode: "user"}};
    navigator.mediaDevices.getUserMedia(constraints)
        .then(function(mediaStream) {
            video.srcObject = mediaStream;})
        .catch(function(error) {
            alert('Camera was blocked');
            console.log(error.name + ": " + error.message);
        });
} else {
    alert('getUserMedia() is not supported in your browser');
}

document.addEventListener("DOMContentLoaded",
    function() {
        document.getElementById("save").addEventListener("click", function() {
            $ajaxUtils.sendGetRequest("/camagru/main", function(){console.log(this.responseText)},
                "img=" + img_src + "&filter=" + filter_src);
            var cur_div = document.querySelector('.selected');
            var img = cur_div.querySelector('img');
            var p = document.createElement('p');
            cur_div.removeChild(img);
            cur_div.style.backgroundImage = 'none';
            cur_div.style.height = '30px';
            cur_div.appendChild(p);
            p.innerHTML = 'The photo has been saved';
            var btn = document.getElementById('save');
            btn.setAttribute('disabled', 'disabled');
            setTimeout(removeDiv, 1000, cur_div);
        });
    });

function removeDiv(cur_div) {
    cur_div.parentElement.removeChild(cur_div);
    }

function drawImage() {
    var new_div = document.createElement('div');
    var new_image = document.createElement('img');
    var photos = document.getElementById('select_photos');
    photos.appendChild(new_div);
    new_div.appendChild(new_image);
    var loaded_img = document.getElementById('loaded_img');
    if (video.style.visibility == 'hidden')
    {
        ctx.clearRect(0, 0, 640, 480);
        ctx.drawImage(loaded_img, 0, 0, 640, 480, 0, 0, 640, 480);
        new_div.style.backgroundImage = "url(" + canvas.toDataURL('image/png') + ")";
    }
    else
    {
        ctx.drawImage(video, 0, 0, 640, 480, 0, 0, 640, 480);
        new_div.style.backgroundImage = 'url("' + canvas.toDataURL('image/png') + '")';
    }
    new_image.src = document.getElementById('filter').src;
    new_div.setAttribute('onclick', 'selectImage(this)');
}

function selectImage(selected_img) {
    var btn = document.getElementById('save');
    btn.removeAttribute('disabled');
    var images = document.getElementById('select_photos').getElementsByTagName('div');
    for (var i = 0, l = images.length; i < l; i++)
    {
        images[i].removeAttribute('class');
        img_src = selected_img.style.backgroundImage;
        filter_src = selected_img.firstChild.src;
    }
    selected_img.className += " selected";
}

function applyFilter(option) {
    var imageSrc = option.firstChild.getAttribute('src');
    var img = document.getElementById('filter');
    img.setAttribute('src', imageSrc);
    var btn = document.getElementById('click');
    btn.removeAttribute('disabled');
}

var uploadImage = function(event) {
    var reader = new FileReader();
    reader.onload = function(theFile) {
        var canvas = document.getElementById('loaded_img');
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, 640, 480);
        var img = new Image();
        img.src = theFile.target.result;
        img.onload = function()
        {
            trimImage(ctx, img, this.width, this.height);
        };
    };
    reader.readAsDataURL(event.target.files[0]);
    video.style.visibility = "hidden";
};

function trimImage(ctx, uploaded_img, sWidth, sHeight) {
    var sx, sy;
    var scale = (640 / sWidth > 480 / sHeight) ? 640 / sWidth : 480 / sHeight;
    var width = sWidth * scale;
    var height = sHeight * scale;
    sx = (width - 640) / 2;
    sy = (height - 480) / 2;
    ctx.drawImage(uploaded_img, sx/scale, sy/scale, 640/scale, 480/scale, 0, 0, 640, 480);
}
