document.addEventListener("DOMContentLoaded",
    function() {
        var photo_likes = document.getElementsByName('like');
        var i;
        var len = photo_likes.length;
        for (i = 0; i < len; i++) {
            photo_likes[i].addEventListener("click", like, false);
        }
    });

function like() {
    var id = this.previousElementSibling.value;
    $ajaxUtils.sendGetRequest("/camagru/photos", function(){},
        "like=true&like_id=" + id);
    var b = this.firstElementChild;
    var num = b.innerHTML;
    num = parseInt(num) + 1;
    b.innerHTML = num;
}