(function (global) {

    var ajaxUtils = {};

    function getRequestObject() {
        if (window.XMLHttpRequest)
            return (new XMLHttpRequest());
        else {
            global.alert("Ajax is not supported");
            return (null);
        }
    }

    ajaxUtils.sendGetRequest =
        function(requestUrl, responseHandler, request_msg) {
            var request = getRequestObject();
            request.onreadystatechange =
                function() {
                    handleResponse(request, responseHandler);
                };
            request.open("POST", requestUrl, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.send(request_msg);

        };

    function handleResponse(request, responseHandler) {
        if ((request.readyState == 4) && (request.status == 200))
        {
            responseHandler(request);
            console.log(request.responseText);
        }
    }

    global.$ajaxUtils = ajaxUtils;

})(window);