<?php
if ($data != null)
	$page = <<<HTML
	<div class="confirm">
		<form action="/camagru/register" method="post">
			<p>{$data}</p>
			<input type="submit" name="home" value="Register">
		</form>
	</div>	
HTML;

else if (array_key_exists('sendmail', $_POST))
	$page = <<<HTML
	<div class="confirm">
		<form action="/camagru/" method="post">
			<p>The new password has been sent.<br>Please check your email</p>
			<input type="submit" name="home" value="Back to homepage">
		</form>
	</div>
HTML;

else
	$page = <<<HTML
<div class="reg form mail">
    <p>The new password will be sent by the email</p>
    <form method="post" action="/camagru/sendpass">
        <label for="email">Your email</label><input id ="email" type="email" name="email"><br>
        <input type="submit" name="sendmail" value="OK">
    </form>
</div>
HTML;

echo $page;
