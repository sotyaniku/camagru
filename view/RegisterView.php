<?php
if ($data != null)
	$page = <<<HTML
	<div class="confirm">
		<form action="/camagru/register" method="post">
			<p>{$data}</p>
			<input type="submit" name="home" value="Back to registration">
		</form>
	</div>
HTML;

else if (array_key_exists('register', $_POST))
	$page = <<<HTML
	<div class="confirm">
		<form action="/camagru/" method="post">
			<p>To complete the registration please check the email</p>
			<input type="submit" name="home" value="Back to homepage">
		</form>
	</div>
HTML;

else
{
	$page = <<<HTML
<div class="reg form">
    <form name="reg_form" method="post" action="/camagru/register" onsubmit="return validate_form();">
        <label for="log">Login</label><input id ="log" type="text" name="login"><!--
        --><label for="passwd">Password</label><!--
        --><input id ="passwd" type="password" name="passwd"><!--
        --><label for="passwd_2">Confirm password</label><!--
        --><input id ="passwd_2" type="password" name="passwd_2"><!--
        --><label for="email">Email</label><input id ="email" type="email" name="email"><br>
        <input type="submit" name="register" value="OK">
    </form>
	<p id="message"></p>
</div>
HTML;
	$page .= <<<HTML
<script type="text/javascript">
	function validate_form()
	{
	    var valid = false;
	    var msg = '';
	    if (document.reg_form.login.value == "")
        	msg = "Please enter your login";
	    else if (document.reg_form.passwd.value == "")
        	msg = "Please enter your password";
	    else if (document.reg_form.passwd_2.value == "")
	        	msg = "Please confirm your password";
	    else if (document.reg_form.email.value == "")
	        	msg = "Please enter your email";
	    else if (document.reg_form.passwd.value != document.reg_form.passwd_2.value)
	        	msg = "Please enter the same password twice";
	    else if (!check_login(document.reg_form.login.value))
	        msg = "Your login should consist of letters and/or numbers only and be 3 to 8 symbols";
	    else if (!check_password(document.reg_form.passwd.value))
	        msg = "Your password should contain both letters and digits and be 8 symbols minimum";
	    else
	        valid = true;
	    write_message(msg);
	    return valid;
	}
	
	function write_message(msg)
	{
	    var par = document.getElementById('message');
	    par.innerHTML = msg;
	}
	
	function check_login(log)
	{
		var matches = log.match(/^[a-zA-Z0-9]+$/);
		return (matches != null && log.length > 2 && log.length < 9);
	}
	
	function check_password(pass)
	{
	    var letter = /[a-zA-Z]/;
    	var number = /[0-9]/;
    	return (letter.test(pass) && number.test(pass) && pass.length > 7);
	}
</script>
HTML;
}
echo $page;
