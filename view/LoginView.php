<?php
if (isset($_SESSION['forgot_password']) && $_SESSION['forgot_password'] == 1)
	$error_msg = "<a href='/camagru/sendpass'></a><b>Forgot your password?<br>
		Click <a href='/camagru/sendpass'>here</a> for update<br></b>";
else
	$error_msg = "";
if ($_SERVER['REQUEST_URI'] === '/camagru/register' || $_SERVER['REQUEST_URI'] === '/camagru/sendpass')
	$page = '';
else if (!isset($_SESSION['logged_on_user']) || !$_SESSION['logged_on_user'])
{
	$page = <<<HTML
<div class="log-in form">
    <form name="log_form" method="post" action="/camagru/login" onsubmit="return validate_form();">
        <label for="log">Login</label><input id ="log" type="text" name="login"><br><!--
        --><label for="passwd">Password</label><input id ="passwd" type="password" name="passwd">
        <input type="submit" name="submit" value="OK">
    </form>
    <span>{$error_msg}</span>
</div>
HTML;
	$page .= <<<HTML
<script type="text/javascript">
	function validate_form()
	{
	    var valid = true;
	    if (document.log_form.login.value == "" || document.log_form.passwd.value == "")
        	valid = false;	
	    return valid;
	}	
	
</script>
HTML;
}
else
    $page = <<<HTML
<div class="log-in form">
    <form method="post" action="/camagru/login">
        <p>Hello, {$_SESSION['logged_on_user']}!</p>
        <input type="submit" name="submit" value="Logout">
    </form>
</div>
HTML;
$_SESSION['uri'] = $_SERVER['REQUEST_URI'];
echo $page;
