<div class="content">
    <div class="main">
        <div class="webcam">
            <video autoplay poster="/camagru/webroot/images/filters/placeholder.png"></video>
            <canvas id="loaded_img" width="640" height="480"></canvas>
            <img id="filter" src="">
            <div>
                <button disabled onclick="drawImage()" id="click">Add photo</button>
                <label for="file"><span>Choose file</span></label><input id="file" type="file" name="file" accept="image/*" onchange="uploadImage(event);">
            </div>
        </div>
        <div class="images">
            <h4>Apply some magic to your photo. Select one of the images below: </h4>
            <form name="images">
                <div>
                    <input type="radio" id="tree" name="pic" value="tree">
                    <label for="tree" onclick="applyFilter(this)"><img src="/camagru/webroot/images/filters/tree.png"></label>
                </div>
                <div>
                    <input type="radio" id="rainbow" name="pic" value="rainbow">
                    <label for="rainbow" onclick="applyFilter(this)"><img src="/camagru/webroot/images/filters/rainbow.png"></label>
                </div>
                <div>
                    <input type="radio" id="hat" name="pic" value="hat">
                    <label for="hat" onclick="applyFilter(this)"><img src="/camagru/webroot/images/filters/hat.png"></label>
                </div>
                <div>
                    <input type="radio" id="cat" name="pic" value="cat">
                    <label for="cat" onclick="applyFilter(this)"><img src="/camagru/webroot/images/filters/cat.png"></label>
                </div>
            </form>
        </div>
        <div class="clear"></div>
    </div>
    <div class="sidebar">
        <h4>Your photos will be shown here. Select the best one!</h4>
        <canvas  id="hidden" width="640" height="480"></canvas>
        <form id="select_photos" method="post">
            <input type="hidden" id="img_source" name="img_source">
            <input type="hidden" id="filter_source" name="filter_source">
        </form>
        <button disabled id="save">Save photo</button>
    </div>
</div>
<script src="/camagru/webroot/js/ajax.js"></script>
<script src="/camagru/webroot/js/main.js"></script>
