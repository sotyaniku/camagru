<div class="photos">
    <?php foreach ($data as $photoItem) :?>
    <div>
        <a href="photos/<?php echo $photoItem['id']?>"><img src="<?php echo $photoItem['src']?>"></a>
        <span><?php echo $photoItem['user']?></span>
        <span><i><?php echo date('y-m-d', strtotime($photoItem['time']))?></i>
			<?php if (isset($_SESSION['logged_on_user']) && $photoItem['user'] == $_SESSION['logged_on_user'])
            {
			    $buttons = <<<HTML
			    <form method="post">
			        <input type="hidden" name="del_id" value="{$photoItem['id']}">
                    <button type="submit" name="delete"></button>
                </form>                
HTML;
            echo $buttons;
            }?>
			<?php if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
			{
				$buttons = <<<HTML
                <span>
                    <input type="hidden" name="like_id" value="{$photoItem['id']}">
                    <button type="submit" name='like'>
                        <b>{$photoItem['like']}</b>
                    </button>
                </span>
HTML;
				echo $buttons;
			}?>
            </span>
    </div>
    <?php endforeach;?>
</div>
<div class="pages">
    <?php
        for ($i = 1, $n = $_SESSION['pages_num']; $i <= $n; $i++)
            if ($_SESSION['cur_page'] != $i)
                echo "<a href='?page=" . $i . "'>" . $i . "</a>";
            else
				echo "<span>" . $i . "</span>";
    ?>
</div>
<script src="/camagru/webroot/js/ajax.js"></script>
<script src="/camagru/webroot/js/photo.js"></script>
