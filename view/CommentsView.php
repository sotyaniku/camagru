<div class="one-photo">
    <div>
        <img src="<?php echo $data[0]['src']?>">
        <span><?php echo $data[0]['user']?></span>
        <span><i><?php echo $data[0]['time']?></i>
			<?php if (isset($_SESSION['logged_on_user']) && $data[0]['user'] == $_SESSION['logged_on_user'])
			{
				$button = <<<HTML
			    <form method="post">
			        <input type="hidden" name="del_id" value="{$data[0]['id']}">
                    <button type="submit" name="delete"></button>
                </form>
HTML;
				echo $button;
			}?>
			<?php if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
			{
				$button = <<<HTML
			    <form method="post">
                    <input type="hidden" name="like_id" value="{$data[0]['id']}">
                        <button type="submit" name='like'>
                    <b>{$data[0]['like']}</b>
                </button>
            </form>
HTML;
				echo $button;
			}?>
        </span>
		<div class="comments">
			<?php
			$add_comment = "";
			if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
				$add_comment = <<<HTML
            <div class="add-comment">
                <form action="" method="post">
                    <h5>Write your comment here:</h5>
                    <textarea wrap="soft" name="comment" autofocus></textarea>
                    <input type="submit" name="add_comment" value="Add comment">
                </form>
            </div>
HTML;
			echo $add_comment;
			?>
			<?php foreach ($data as $comment) :?>

            <div class="div">
                <span class="comment-author"><?php echo $comment['comment_login']?></span>
                <span class="comment-time"><?php echo " (" . $comment['comment_time'] . ")"?></span>
                <p class="comment-text"><?php echo $comment['comment_text']?></p>
            </div>
			<?php endforeach;?>
        </div>
    </div>
</div>
<script>
    var par = document.querySelector('.comment-text');
    var div = document.querySelector('.div');
    if (par.innerHTML == "")
        div.parentNode.removeChild(div);
</script>
