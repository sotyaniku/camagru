<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="/camagru/webroot/css/style.css">
        </head>
	<body>
        <div>
            <menu>
                <div>
                    <ul class="menu">
                        <li><a href="/camagru/photos">Gallery</a></li><!--
                    --><li><a href="/camagru/main">Profile</a></li><!--
                    --><li><a href="/camagru/register">Register</a></li><!--
                    --><li><header>
								<?php include 'view/LoginView.php'; ?>
                            </header></li>

                    </ul>

                </div>
            </menu>
    	    <?php include 'view/' . $content_view; ?>
            <footer>Developed by &copy; ksarnyts</footer>
        </div>
    </body>
</html>
