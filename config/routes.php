<?php

const ROUTES = array (
	'photos/([a-z0-9]+)' => 'photo/view/$1',
	'photos\?(.+)' => 'photo/index',
	'photos' => 'photo/index',
	'main' => 'user/index',
	'login' => 'user/login',
	'register/([a-z0-9])' => 'user/confirm/$1',
	'register' => 'user/register',
	'sendpass' => 'user/sendpass',
	'camagru' => 'camagru/photo/index'
);
