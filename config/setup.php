#!/usr/bin/php
<?php
$paramsPath = 'database.php';
include($paramsPath);

try {
	$db = new PDO('mysql:host=127.0.0.1', $DB_USER, $DB_PASSWORD);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	echo 'Connection failed: ' . $e->getMessage();
}

//$db = new PDO('mysql:host=127.0.0.1', $DB_USER, $DB_PASSWORD);
$sql = $db->prepare("CREATE DATABASE IF NOT EXISTS camagru");
$sql->execute();
$db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
$sql = $db->prepare(file_get_contents("camagru.sql"));
$sql->execute();
?>