<?php

class Router
{
	private $_routes;

	public function __construct($routes)
	{
		$this->_routes = $routes;
	}

	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI']))
			return trim($_SERVER['REQUEST_URI'], '/');
	}

	public function run()
	{
		$uri = $this->getURI();
		foreach ($this->_routes as $pattern => $path)
		{
			if (preg_match("@$pattern@", $uri))
			{
				$internalRoute = preg_replace("@$pattern@", $path, $uri);
				$segments = explode('/', $internalRoute);
				array_shift($segments);
				$controllerName = ucfirst(array_shift($segments)) . 'Controller';
				$actionName = 'action' . ucfirst(array_shift($segments));
				$controllerFile = ROOT . '/controller/' . $controllerName . '.php';
				if (file_exists($controllerFile))
					require_once $controllerFile;
				$controllerObject = new $controllerName();
				$result = call_user_func_array(array ($controllerObject, $actionName), $segments);
				if ($result !== 0)
					break;
			}
		}
	}
}
