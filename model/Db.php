<?php
class Db
{
    public static $pdo;

	public static function getConnection()
	{
		//$params = include($paramsPath);
		//$dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
		//$db = new PDO($dsn, $params['user'], $params['password']);

        if (!self::$pdo) {


            self::$pdo = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
        }

        return self::$pdo;
	}
}
