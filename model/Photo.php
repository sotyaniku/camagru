<?php
require_once ROOT . '/model/Db.php';

class Photo
{
	public static function getPhotoItemById($id)
	{
		$id = intval($id);

		if ($id)
		{
			$db = Db::getConnection();

			$photoList = array();
			$result = $db->query('SELECT tbl_photo.photo_id, photo_user, photo_src, photo_time, photo_like, '
				. 'comment_id, comment_time, comment_text, user_login '
				. 'FROM tbl_photo LEFT JOIN tbl_comment '
				. 'ON tbl_photo.photo_id=tbl_comment.photo_id '
				. 'WHERE tbl_photo.photo_id =' . $id
				. ' ORDER BY comment_id DESC');
			$i = 0;
			while ($row = $result->fetch())
			{
				$photoList[$i]['id'] = $row['photo_id'];
				$photoList[$i]['user'] = $row['photo_user'];
				$photoList[$i]['src'] = $row['photo_src'];
				$photoList[$i]['time'] = $row['photo_time'];
				$photoList[$i]['like'] = $row['photo_like'];
				$photoList[$i]['comment_id'] = $row['comment_id'];
				$photoList[$i]['comment_time'] = $row['comment_time'];
				$photoList[$i]['comment_text'] = $row['comment_text'];
				$photoList[$i]['comment_login'] = $row['user_login'];
				$i++;
			}
			return $photoList;
		}
	}

	public static function getPhotoById($photo_id)
	{
		$db = Db::getConnection();

		$sql = "SELECT * FROM tbl_photo WHERE photo_id = :photo_id";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"photo_id" => $photo_id
		));
		$sth->setFetchMode(PDO::FETCH_ASSOC);
		$photo = $sth->fetch();
		return $photo;
	}

	public static function getPhotoNum()
	{
		$db = Db::getConnection();

		$sql = "SELECT count(*) FROM tbl_photo";
		$res = $db->query($sql);
		$row = $res->fetch();
		$total_rows = $row[0];
		return ($total_rows);
	}

	public static function getPhotoList($start_num, $per_page_num)
	{
		$db = Db::getConnection();

		$photoList = array();
		$result = $db->query('SELECT photo_id, photo_user, photo_src, photo_time, photo_like '
		. 'FROM tbl_photo '
		. 'ORDER BY photo_time DESC LIMIT ' . $start_num . ',' . $per_page_num);
		$i = 0;
		while ($row = $result->fetch())
		{
			$photoList[$i]['id'] = $row['photo_id'];
			$photoList[$i]['user'] = $row['photo_user'];
			$photoList[$i]['src'] = $row['photo_src'];
			$photoList[$i]['time'] = $row['photo_time'];
			$photoList[$i]['like'] = $row['photo_like'];
			$i++;
		}
		return $photoList;
	}

	public static function addPhoto($login, $src, $cur_time)
	{
		$db = Db::getConnection();

		$sql = "INSERT INTO tbl_photo (photo_user, photo_src, photo_time) 
			VALUES(:login, :src, :cur_time)";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"login" => $login,
			"src" => $src,
			"cur_time" => $cur_time
		));
	}

	public static function addCommentToPhoto($photo_id, $comment_text, $login, $cur_time)
	{
		$db = Db::getConnection();

		$sql = "INSERT INTO tbl_comment (photo_id, comment_time,
			comment_text, user_login) VALUES(:id, :cur_time, :text, :login)";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"id" => $photo_id,
			"login" => $login,
			"text" => $comment_text,
			"cur_time" => $cur_time
		));
	}

	public static function deletePhotoFromDb($photo_id, $photo_user)
	{
		$db = Db::getConnection();

		$sql = "DELETE FROM tbl_photo WHERE photo_id = :photo_id AND photo_user = :photo_user";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"photo_id" => $photo_id,
			"photo_user" => $photo_user
		));
	}

	public static function addLikeToPhoto($photo_id)
	{
		$db = Db::getConnection();

		$sql = "UPDATE tbl_photo SET photo_like = photo_like + 1 WHERE photo_id = :photo_id";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"photo_id" => $photo_id
		));
	}
}
