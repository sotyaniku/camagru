<?php

require_once ROOT . '/model/Db.php';

class User
{
	public static function getUserByEmail($email)
	{
		$db = Db::getConnection();

		$sql = "SELECT * FROM tbl_user WHERE user_email = :email";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"email" => $email
		));
		$sth->setFetchMode(PDO::FETCH_ASSOC);
		$user = $sth->fetch();
		return $user;
	}

	public static function getUserByLogin($login)
	{
		$db = Db::getConnection();

		$sql = "SELECT * FROM tbl_user WHERE user_login = :login";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"login" => $login
		));
		$sth->setFetchMode(PDO::FETCH_ASSOC);
		$user = $sth->fetch();
		return $user;
	}

	public static function getUserList()
	{
		$db = Db::getConnection();

		$userList = array();
		$result = $db->query('SELECT user_login, user_password, user_email, registered '
		. 'FROM tbl_user');
		$i = 0;
		while ($row = $result->fetch())
		{
			$userList[$i]['login'] = $row['user_login'];
			$userList[$i]['password'] = $row['user_password'];
			$userList[$i]['email'] = $row['user_email'];
			$userList[$i]['registered'] = $row['registered'];
			$i++;
		}
		return $userList;
	}

	public static function addUserToDb($login, $password, $email, $flag)
	{
		$db = Db::getConnection();
		$sql = "INSERT INTO tbl_user (user_login, user_password, user_email, registered) 
			VALUES(:login, :password, :email, :flag)";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"login" => $login,
			"password" => $password,
			"email" => $email,
			"flag" => $flag
		));
	}

	public static function confirmUserRegistration($login)
	{
		$db = Db::getConnection();
		$sql = "UPDATE tbl_user SET registered = 1 WHERE user_login = :login";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"login" => $login
		));
	}

	public static function updateUserPassword($login, $password)
	{
		$db = Db::getConnection();
		$sql = "UPDATE tbl_user SET user_password = :password WHERE user_login = :login";
		$sth = $db->prepare($sql);
		$sth->execute(array(
			"login" => $login,
			"password" => $password
		));
	}
}
